#!/bin/bash

echo "deleting loadbalancer $1"
for l in $(openstack loadbalancer show $1 -c listeners -f value); do
    echo "  deleting listener $l"
    pool=$(openstack loadbalancer listener show -c default_pool_id $l -f value)
    echo "    deleting pool $pool"
    for m in $(openstack loadbalancer member list -c id -f value $pool); do
        echo "      deleting member $pool $m"
        openstack loadbalancer member delete $pool $m
    done
    healthmon=$(openstack loadbalancer pool show $pool -c healthmonitor_id -f value)
    if [ -n $healthmon ]; then
        openstack loadbalancer healthmonitor delete $healthmon
    fi
    openstack loadbalancer pool delete $pool
    openstack loadbalancer listener delete $l
done
openstack loadbalancer delete $1
