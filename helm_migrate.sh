#!/bin/bash

clear
echo "######################################################################"
echo "Welcome to the helm charts migration tool"
echo "This tool will help you to migrate your charts in registry.cern.ch"
echo "under chartmuseum to the harbor registry as OCI artifacts"
echo ""
echo "PLEASE NOTE THAT ANY HELM CHART PATH:TAG MATCHING A CONTAINER PATH:TAG"
echo "WILL OVERWRITE THE EXISTENT CONTAINER TAG."
echo "######################################################################"
echo ""
echo ""
## SELECT WHICH PROJECT TO CONVERT
read -p "Please type the registry.cern.ch project where your charts are [magnum]: " REGISTRY_PROJECT_NAME
REGISTRY_PROJECT_NAME=${REGISTRY_PROJECT_NAME:-magnum}
## EXTRA CHART PATH
read -p "Please paste any additional path on which you wish to organize your charts. Ex: foo/bar or [charts].
The full path will become registry.cern.ch/${REGISTRY_PROJECT_NAME}/charts/: " REGISTRY_CHART_SUBPATH
REGISTRY_CHART_SUBPATH=${REGISTRY_CHART_SUBPATH:-charts}
read -p "Do you wish to overwrite existent OCI charts [false]: " CHART_OVERWRITE
CHART_OVERWRITE=${CHART_OVERWRITE:-false}
echo ""
## USER CREDENTIALS
read -p "Please type your username [${REGISTRY_PROJECT_NAME}]: " REGISTRY_USERNAME
read -s -p "Please paste your user's harbor CLI secret: " REGISTRY_SECRET
echo ""
REGISTRY_USERNAME=${REGISTRY_USERNAME:-${REGISTRY_PROJECT_NAME}}


## Login to registry
echo ${REGISTRY_SECRET} | helm registry login registry.cern.ch \
  --username ${REGISTRY_USERNAME} \
  --password-stdin
if [[ "$?" != 0 ]]; then
	exit -1
fi

## Download and commit
echo ""
echo "Scanning project charts"
DIR_ROOT="$(mktemp -d)/migrate-helm-script"
mkdir -p ${DIR_ROOT}/${REGISTRY_PROJECT_NAME}
DATA_JSON=$(curl "https://registry.cern.ch/api/chartrepo/${REGISTRY_PROJECT_NAME}/charts" \
  -H 'accept: application/json' \
  -H 'content-type: application/json' \
  -u "${REGISTRY_USERNAME}:${REGISTRY_SECRET}" \
  --compressed --silent)
HELM_CHARTS_LIST=$(echo ${DATA_JSON} | jq  '.[].name')

for chart in ${HELM_CHARTS_LIST}; do
	echo ""
	echo ""
	echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
	echo "Converting versions of ${chart} helm chart."
	chart=$(echo $chart | tr -d '"')
	mkdir -p ${DIR_ROOT}/${REGISTRY_PROJECT_NAME}/${chart}
	data_json=$(curl "https://registry.cern.ch/api/chartrepo/${REGISTRY_PROJECT_NAME}/charts/$chart" \
	  -H 'accept: application/json' \
	  -H 'content-type: application/json' \
	  -u "${REGISTRY_USERNAME}:${REGISTRY_SECRET}" \
	  --compressed --silent)

	NUM_CHART_VERSIONS=0
	for url in $(echo $data_json | jq -r '.[].urls[]'); do
		CHART="${DIR_ROOT}/${REGISTRY_PROJECT_NAME}/$chart/${url#charts\/}"
		echo ""
		## Check if OCI chart already exists
		VERSION=$(echo $data_json | jq --arg v "${url}" '.[] | select( .urls | index( $v ) ) | .version ' -r)
		helm pull oci://registry.cern.ch/${REGISTRY_PROJECT_NAME}/${REGISTRY_CHART_SUBPATH}/${chart} --version ${VERSION} --destination ${DIR_ROOT}/${REGISTRY_PROJECT_NAME}/${chart} > /dev/null 2>&1
		if [[ "$?" == 0 ]]; then
			if [[ ${CHART_OVERWRITE} == "true" ]]; then
				echo "Overwritting chart $(basename ${CHART})."
			else
				echo "Chart $(basename ${CHART}) already exists, skipping."
				continue
			fi
		fi
		## Pull chart from chartmuseum
		echo "Downloading to ${CHART}"
		curl "https://registry.cern.ch/chartrepo/${REGISTRY_PROJECT_NAME}/$url" \
		  -o "${CHART}" \
		  -H 'accept: application/json, text/plain, */*' \
		  -u "${REGISTRY_USERNAME}:${REGISTRY_SECRET}" \
		  --compressed --silent
		## Push chart to OCI registry
		echo "Pushing $(basename ${CHART}) to regisecho "Clean"try.cern.ch/${REGISTRY_PROJECT_NAME}/${REGISTRY_CHART_SUBPATH}"
		helm push ${CHART} oci://registry.cern.ch/${REGISTRY_PROJECT_NAME}/${REGISTRY_CHART_SUBPATH}
		if [[ $? == 0 ]]; then
			NUM_CHART_VERSIONS=$((NUM_CHART_VERSIONS+1))
		fi
	done

	echo ""
	echo "Converted versions: ${NUM_CHART_VERSIONS}"
done

echo ""
echo ""
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"
rm $DIR_ROOT -rf
echo "Local resources clean. All done."