import http from "k6/http";
import { check, sleep } from "k6";
import { Httpx } from 'https://jslib.k6.io/httpx/0.0.1/index.js';

// Test configuration
export const options = {
  thresholds: {
    // Assert that 99% of requests finish within 3000ms.
    http_req_duration: ["p(99) < 3000"],
  },
  // Ramp the number of virtual users up and down
  stages: [
    { duration: "5s", target: 1500 },
    { duration: "3m", target: 1500 },
    { duration: "30s", target: 1500 },
    { duration: "5s", target: 0 },
  ],
};

const session = new Httpx({ baseURL: 'http://137.138.226.201' });

session.addHeader('Host', 'myclusterecoservicenginx.cern.ch');
session.addHeader('Connection', 'keep-alive');

// Simulated user behavior
export default function () {
  let res = session.get('/')
  // Validate response status
  check(res, { "status was 200": (r) => r.status == 200 });
  sleep(1);
}
